from datetime import datetime
global snowflake
while True:
    while True:
        inpt = input("\nInput the ID of a message (also known as \"snowflake\")\n")
        if len(inpt)==18 and inpt.isdigit():
            snowflake = str(bin(int(inpt)))[2:]
            break
        else:
            print("Invalid input.\n")
    try:
        binaryTimestamp = str(snowflake)[:-22]
        # The +1420070400000 is the Discords' Epoch, which is the first second of the year 2015
        utcTime = (int(binaryTimestamp,2)+1420070400000)/1000
    except:
        print("Invalid timestamp.")
        break
    print("{}{}".format("The snowflake in binary:           ",snowflake))
    print("{}{}".format("UTC time in miliseconds:           ",utcTime))
    print("{}{}".format("The time at which it was sent:     ",datetime.fromtimestamp(utcTime).strftime('%Y-%m-%d %H:%M:%S.%f')))
    print("{}{}".format("Internal worker ID:                ", snowflake[39:44]))
    print("{}{}".format("Internal process ID:               ", snowflake[45:50]))
    print("{}{}".format("Increment of all IDs generated from this process: ", snowflake[51:]))